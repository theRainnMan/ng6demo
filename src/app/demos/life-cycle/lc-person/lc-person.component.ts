import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck, Input,
  OnChanges,
  OnDestroy,
  OnInit, SimpleChanges
} from '@angular/core';
import {Person} from '../../models/Person';

@Component({
  selector: 'app-lc-person',
  templateUrl: './lc-person.component.html',
  styleUrls: ['./lc-person.component.css']
})
export class LcPersonComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked,
  AfterViewInit, AfterViewChecked, OnDestroy {

  @Input()
  person: Person;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.log(new Date() + '- ngOnChanges', changes);
  }

  ngOnInit(): void {
  }

  ngDoCheck() {
    console.log(new Date() + '- ngDoCheck');
  }

  ngAfterContentInit() {
    console.log(new Date() + '- ngAfterContentInit');
  }
  ngAfterContentChecked() {
    console.log(new Date() + '- ngAfterContentChecked');
  }
  ngAfterViewInit() {
    console.log(new Date() + '- ngAfterViewInit');
  }
  ngAfterViewChecked() {
    console.log(new Date() + '- ngAfterViewChecked');
  }
  ngOnDestroy() {
    console.log(new Date() + '- ngOnDestroy');
  }
}
