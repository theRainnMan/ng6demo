import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ve-native',
  templateUrl: './ve-native.component.html',
  styleUrls: ['./ve-native.component.css'],
  // encapsulation: ViewEncapsulation.Native
})
export class VeNativeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
