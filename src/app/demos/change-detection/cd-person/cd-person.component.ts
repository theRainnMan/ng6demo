import {AfterViewChecked, Component, DoCheck, Input, OnChanges, OnInit} from '@angular/core';
import {ImPerson} from '../../models/im-person';

@Component({
  selector: 'app-cd-person',
  templateUrl: './cd-person.component.html',
  styleUrls: ['./cd-person.component.css']
})
export class CdPersonComponent implements OnInit, OnChanges, DoCheck, AfterViewChecked {

  @Input()
  personCD: ImPerson;


  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    console.log(new Date() + '- ngOnChanges in cd-person');
  }

  ngDoCheck() {
    console.log(new Date() + '- ngDoCheck in cd-person');
  }

  ngAfterViewChecked() {
    console.log(new Date() + '- ngAfterViewChecked in cd-person');
  }
}
