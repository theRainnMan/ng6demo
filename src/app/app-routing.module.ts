import {RouterModule, Routes} from '@angular/router';
import {LifeCycleComponent} from './demos/life-cycle/life-cycle.component';
import {NgModule} from '@angular/core';
import {ChangeDetectionComponent} from './demos/change-detection/change-detection.component';
import {ViewEncapsulationComponent} from './demos/view-encapsulation/view-encapsulation.component';
import {FormComponent} from './demos/form/form.component';
import {DirectiveComponent} from './demos/directive/directive.component';

const routes: Routes = [
  {
    path: 'lc',
    component: LifeCycleComponent
  },
  {
    path: 'cd',
    component: ChangeDetectionComponent
  },
  {
    path: 've',
    component: ViewEncapsulationComponent
  },
  {
    path: 'f',
    component: FormComponent
  },
  {
    path: 'd',
    component: DirectiveComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
